package hpmanager

import (
	"context"

	"github.com/rs/zerolog/log"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	hp "gitlab.com/johnrichter/house-points"
	hpeventbus "gitlab.com/johnrichter/house-points/event/bus"
	"gitlab.com/johnrichter/logging-go"
	"gitlab.com/johnrichter/slack-app"
	"gitlab.com/johnrichter/tracing-go"
	traceext "gopkg.in/DataDog/dd-trace-go.v1/ddtrace/ext"
	"gopkg.in/DataDog/dd-trace-go.v1/ddtrace/tracer"
)

type HousePointsService struct {
	slack    *slack.Client
	hpc      *hp.HousePointsConfig
	eventBus hpeventbus.EventBusProducer
}

var (
	hplog = log.With().Str(logging.SlackServiceName, hp.HousePointsServiceName).Logger()
)

func NewHousePointsService(
	slackConfig *slackapp.SlackConfig,
	hpConfig *hp.HousePointsConfig,
	eventBus hpeventbus.EventBusProducer,

) *HousePointsService {
	hps := HousePointsService{
		slack:    slack.New(slackConfig.SlackBotToken, slack.OptionHTTPClient(tracing.SlackHttpClient)),
		hpc:      hpConfig,
		eventBus: eventBus,
	}
	return &hps
}

func (s *HousePointsService) ServiceName() string {
	return hp.HousePointsServiceName
}

func (s *HousePointsService) SlashCommands() []string {
	return housePointsEnabledSlashCommands
}

func (s *HousePointsService) InteractionIDs() []string {
	return housePointsEnabledInteractions
}

func (s *HousePointsService) EventTypes() []string {
	return housePointsEnabledEventTypes
}

func (s *HousePointsService) HandleInteraction(
	ctx context.Context,
	ic *slack.InteractionCallback,
) (*slackapp.SlackCallbackResponse, error) {

	span, _ := tracer.SpanFromContext(ctx)
	span.SetTag(traceext.ResourceName, "slack.interaction")
	defer span.Finish()

	switch ic.CallbackID {
	case hp.HousePointsInteractionIDGlobal, hp.HousePointsInteractionIDMessage:
		return s.handleSupportedInteraction(ctx, ic)
	}
	hplog.Debug().Str(logging.SlackInteractionID, ic.CallbackID).Msg("Unknown slack interaction ID")
	return &slackapp.SuccessNoContent, nil
}

func (s *HousePointsService) HandleSlashCommand(
	ctx context.Context,
	cmd *slack.SlashCommand,
) (*slackapp.SlackCallbackResponse, error) {

	span, _ := tracer.SpanFromContext(ctx)
	span.SetTag(traceext.ResourceName, "slack.slash_command.house_points")
	defer span.Finish()

	switch cmd.Command {
	case hp.HousePointsSlashCommandPoints:
		return s.handleSlashPointsCommand(ctx, cmd)
	}
	hplog.Debug().Str(logging.SlackSlashCommandName, cmd.Command).Msg("Unknown House Points slash command")
	return &slackapp.SuccessNoContent, nil
}

func (s *HousePointsService) HandleEvent(
	ctx context.Context,
	ce *slackevents.EventsAPICallbackEvent,
	ie *slackevents.EventsAPIInnerEvent,
) {
	span, _ := tracer.SpanFromContext(ctx)
	span.SetTag(traceext.ResourceName, "slack.event.house_points")
	defer span.Finish()

	var err error
	switch e := ie.Data.(type) {
	case *slackevents.LinkSharedEvent:
		err = s.handleSlackLinkSharedEvent(ctx, ce, e)
	case *slackevents.ReactionAddedEvent:
		err = s.handleSlackReactionAddedEvent(ctx, ce, e)
	case *slackevents.ReactionRemovedEvent:
		err = s.handleSlackReactionRemovedEvent(ctx, ce, e)
	default:
		log.Info().Str(logging.SlackEventType, ie.Type).Msg("Unknown Slack event")
	}
	if err != nil {
		hplog.Error().Err(err).Str(logging.SlackEventType, ie.Type).Msg("Unable to handle Slack event")
	}
}
