package hpmanager

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/rs/zerolog/log"
	"github.com/slack-go/slack"
	tracing "gitlab.com/johnrichter/tracing-go"
)

func PostSlackMsgToURL(ctx context.Context, url string, msg *slack.Msg) (*http.Response, error) {
	msgBytes, err := json.Marshal(&msg)
	if err != nil {
		log.Error().Err(err).Msg("Tried to send malformed Slack message")
		return nil, errors.New("malformed Slack Msg")
	}
	return tracing.DoSlackApi(ctx, http.MethodPost, url, bytes.NewReader(msgBytes))
}
