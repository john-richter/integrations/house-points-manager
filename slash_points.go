package hpmanager

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/slack-go/slack"
	"gitlab.com/johnrichter/slack-app"
)

const (
	slashPointsCmdHelp        = "help"
	slashPointsCmdPlay        = "play"        // No args also uses the dialog
	slashPointsCmdLeaderboard = "leaderboard" // Print a link to the leaderboard
	slashPointsCmdWebsite     = "website"     // Print a link to the point submission page on the GTM website
	slashPointsCmdEmpty       = ""
)

var (
	slackMsgUnknownCommandText = fmt.Sprintf(
		"You've entered a command I don't understand; try `%s`, `%s`, `%s`, or `%s`?",
		slashPointsCmdHelp, slashPointsCmdPlay, slashPointsCmdLeaderboard, slashPointsCmdWebsite,
	)
	respSlashPointsHelp = slackapp.SlackCallbackResponse{
		StatusCode: http.StatusOK,
		Data: slack.Msg{
			ResponseType: "ephemeral",
			Attachments: []slack.Attachment{
				{
					Fallback:   "Overview of what `/points` does",
					Color:      "good",
					AuthorName: "House Points",
					Title:      "Overview",
					Text:       "Welcome to the GTM House Points game!\n\nThis command enables you to give or take away points from GTM member on Slack!\n\nJust type `/points` and you'll be presented with a dialog to submit your points. It is that easy. A description of other ways to use the command is below.",
				},
				{
					Fallback: "Overview of what `/points play` does",
					Color:    "good",
					Title:    "Play",
					Text:     "The `play` command will bring up a dialog you can use to submit your points.",
				},
				{
					Fallback: "Overview of what `/points leaderboard` does",
					Color:    "good",
					Title:    "Leaderboard",
					Text:     "The `leaderboard` command, i.e. `/points leaderboard` will print a link to the leaderboard on the website that you can use to view the rankings and recent activity in the House Points game.",
				},
				{
					Fallback: "Overview of what `/points website` does",
					Color:    "good",
					Title:    "Website Submission",
					Text:     "The `website` command, i.e. `/points website` will print a link for the House Points submission page on the website.\n\nCurrently there is no way to award points to entities like `Road Atlanta` within Slack. This acts as a shortcut to the point submission page so you can award such entities in the House Points game.",
				},
			},
		},
	}
	respSlashPointsUnknownCommand = slackapp.SlackCallbackResponse{
		StatusCode: http.StatusOK,
		Data: slack.Msg{
			ResponseType: "ephemeral",
			Attachments: []slack.Attachment{
				{
					AuthorName: "House Points",
					Color:      "danger",
					Text:       slackMsgUnknownCommandText,
					Fallback:   slackMsgUnknownCommandText,
				},
			},
		},
	}
)

type SlashPointsArgs struct {
	subcommand string
}

func NewSlashPointsArgs(text string) *SlashPointsArgs {
	subcommand := ""
	textArgs := strings.Split(text, " ")
	if len(textArgs) > 0 {
		subcommand = textArgs[0]
	}
	return &SlashPointsArgs{subcommand: subcommand}
}

func (s *HousePointsService) handleSlashPointsCommand(
	ctx context.Context,
	cmd *slack.SlashCommand,
) (*slackapp.SlackCallbackResponse, error) {
	args := NewSlashPointsArgs(cmd.Text)
	switch args.subcommand {
	case slashPointsCmdEmpty, slashPointsCmdHelp:
		return s.slashPointsHelp()
	case slashPointsCmdPlay:
		return s.slashPointsPlay(ctx, cmd.TriggerID)
	case slashPointsCmdLeaderboard:
		return s.slashPointsLeaderboard()
	case slashPointsCmdWebsite:
		return s.slashPointsWebsite()
	}
	return s.slashPointsUnknown()
}

func (s *HousePointsService) slashPointsHelp() (*slackapp.SlackCallbackResponse, error) {
	return &respSlashPointsHelp, nil
}

func (s *HousePointsService) slashPointsPlay(
	ctx context.Context,
	triggerID string,
) (*slackapp.SlackCallbackResponse, error) {
	return s.openGlobalPointSubmisionDialog(ctx, triggerID, "", "")
}

func (s *HousePointsService) slashPointsLeaderboard() (*slackapp.SlackCallbackResponse, error) {
	text := fmt.Sprintf(
		"To see the current standings please visit the <%s|House Points Leaderboard>",
		s.hpc.HousePointsLeaderboardURL,
	)
	fallback := fmt.Sprintf("To see the current standings please visit the %s", s.hpc.HousePointsLeaderboardURL)
	resp := slackapp.SlackCallbackResponse{
		StatusCode: http.StatusOK,
		Data: slack.Msg{
			ResponseType: "ephemeral",
			Attachments: []slack.Attachment{
				{
					AuthorName: "House Points",
					Color:      "good",
					Text:       text,
					Fallback:   fallback,
				},
			},
		},
	}
	return &resp, nil
}

func (s *HousePointsService) slashPointsWebsite() (*slackapp.SlackCallbackResponse, error) {
	text := fmt.Sprintf(
		"To submit points for a wider variety of things visit the <%s|House Points Submission Page> on the GTM website",
		s.hpc.HousePointsManualSubmissionURL,
	)
	fallback := fmt.Sprintf(
		"To submit points for a wider variety of things visit %s",
		s.hpc.HousePointsManualSubmissionURL,
	)
	resp := slackapp.SlackCallbackResponse{
		StatusCode: http.StatusOK,
		Data: slack.Msg{
			ResponseType: "ephemeral",
			Attachments: []slack.Attachment{
				{
					AuthorName: "House Points",
					Color:      "good",
					Text:       text,
					Fallback:   fallback,
				},
			},
		},
	}
	return &resp, nil
}

func (s *HousePointsService) slashPointsUnknown() (*slackapp.SlackCallbackResponse, error) {
	return s.slashPointsHelp()
}
